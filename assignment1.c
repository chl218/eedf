#include "assignment1.h"
#include <stdio.h>
#include <wiringPi.h>
#include <softPwm.h>
#include <stdint.h>


void debug(SharedVariable *sv) {
   printf("state: %d, button: %d, temp: %d, track: %d, touch: %d \
           red: %d, green: %d, blue %d\n ",
               sv->state,
               !digitalRead(PIN_BUTTON),
               digitalRead(PIN_TEMP),
               !digitalRead(PIN_TRACK),
               digitalRead(PIN_TOUCH),
               digitalRead(PIN_RED),
               digitalRead(PIN_GREEN),
               digitalRead(PIN_BLUE));
}

void init_shared_variable(SharedVariable* sv) {
	// You can initialize the shared variable if needed.
	sv->bProgramExit = 0;
   sv->state = STATE_INIT;

   int i;
   for(i = 0; i < 8; i++) {
      sv->wcet[i]         = 0;
      sv->currDeadline[i] = 0;
      sv->execTime[i]     = 0;
      sv->slack[i]        = 0;
   }

   sv->prevTask = 0;
   sv->prevTime = 0;
}

void init_sensors(SharedVariable* sv) {
   // actuators to board 
   pinMode(PIN_BUTTON, INPUT);
   pinMode(PIN_TEMP,   INPUT);
   pinMode(PIN_TRACK,  INPUT);
   pinMode(PIN_TOUCH,  INPUT);

   // board to actuartors
   pinMode(PIN_YELLOW, OUTPUT);
   pinMode(PIN_ALED,   OUTPUT);
   pinMode(PIN_BUZZER, OUTPUT);
   softPwmCreate(PIN_RED,   0, 0xFF);
   softPwmCreate(PIN_GREEN, 0, 0xFF);
   softPwmCreate(PIN_BLUE,  0, 0xFF);  
}

void body_button(SharedVariable* sv) {

   static int onPressed = 0;


   if(!digitalRead(PIN_BUTTON) && onPressed == 0) {
      onPressed = 1;
      if(sv->state != STATE_INIT) {
         sv->state = STATE_INIT;
      }
      else {
         sv->state = STATE_DRIVE;
      }
   }
	else if(digitalRead(PIN_BUTTON) && onPressed == 1) {
		onPressed = 0;
	}

//    int pressed = 0;
// //   while(!digitalRead(PIN_BUTTON)) {
//       if(pressed == 0) {   // change state once per press
//          if(sv->state != STATE_INIT) {
//             sv->state = STATE_INIT;
//          }
//          else {
//             sv->state = STATE_DRIVE;
//          }
//          pressed = 1;     
//       }
// //   } // end-while
}

void body_twocolor(SharedVariable* sv) {
   switch(sv->state) {
      case STATE_INIT:  digitalWrite(PIN_YELLOW, LOW);   break;
      default:          digitalWrite(PIN_YELLOW, HIGH);
   }
}

void body_temp(SharedVariable* sv) {
   // if(digitalRead(PIN_TEMP) && sv->state == STATE_DRIVE) {
   //    sv->state = STATE_TEMP;
   // }
}

void body_track(SharedVariable* sv) {
   //debug(sv);
   if(!digitalRead(PIN_TRACK) && sv->state == STATE_DRIVE) {
      sv->state = STATE_TRACK;
   }
}

void body_touch(SharedVariable* sv) {
   if(digitalRead(PIN_TOUCH) && sv->state == STATE_DRIVE) {
      sv->state = STATE_TOUCH;
   }
}

void body_rgbcolor(SharedVariable* sv) {

   switch(sv->state) {
      case STATE_INIT: softPwmWrite(PIN_RED,   0x00);
                       softPwmWrite(PIN_GREEN, 0x00);
                       softPwmWrite(PIN_BLUE,  0xFF);
                       break;

      case STATE_DRIVE: softPwmWrite(PIN_RED,   0xFF);
                        softPwmWrite(PIN_GREEN, 0xFF);
                        softPwmWrite(PIN_BLUE,  0x00);
                        break;

      case STATE_TRACK: softPwmWrite(PIN_RED,   0xFF);
                        softPwmWrite(PIN_GREEN, 0x00);
                        softPwmWrite(PIN_BLUE,  0x00);
                        break;

      case STATE_TOUCH: softPwmWrite(PIN_RED,   0xC8);
                        softPwmWrite(PIN_GREEN, 0x3B);
                        softPwmWrite(PIN_BLUE,  0xFF);
                        break;

      default: softPwmWrite(PIN_RED,   digitalRead(PIN_RED));
               softPwmWrite(PIN_GREEN, digitalRead(PIN_GREEN));
               softPwmWrite(PIN_BLUE,  digitalRead(PIN_BLUE));
   }
}

void body_aled(SharedVariable* sv) {
      if(sv->state != STATE_INIT && sv->state != STATE_TRACK && sv->state != STATE_TOUCH) {
         digitalWrite(PIN_ALED, digitalRead(PIN_TEMP));
      }
      else {
         digitalWrite(PIN_ALED, LOW);          
      }
}

void body_buzzer(SharedVariable* sv) {
   switch(sv->state) {
      case STATE_TOUCH: digitalWrite(PIN_BUZZER, digitalRead(PIN_TOUCH));
                        break;
      default:          digitalWrite(PIN_BUZZER, LOW);
   }
}

