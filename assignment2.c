#include "assignment1.h"
#include "assignment2.h"
#include "workload.h"
#include "scheduler.h"
#include "governor.h"


#define PROFILE_AMOUNT 50

#define DEBUG          0

// Note: Deadline of each workload is defined in the "workloadDeadlines" variable.
// i.e., You can access the dealine of the BUTTON thread using workloadDeadlines[BUTTON]
// See also deadlines.c and workload.h

// Assignment: You need to implement the following two functions.

// learn_workloads(SharedVariable* v):
// This function is called at the start part of the program before actual scheduling
// - Parameters
// sv: The variable which is shared for every function over all threads
void learn_workloads(SharedVariable* sv) {
	// TODO: Fill the body
	// This function is executed before the scheduling simulation.
	// You need to calculate the execution time of each thread here.

	// Thread functions for workloads: 
	// thread_button, thread_twocolor, thread_temp, thread_track,
	// thread_touch, thread_rgbcolor, thread_aled, thread_buzzer

	// Tip 1. You can call each workload function here like:
	// thread_button();

	// Tip 2. You can get the current time here like:
	// long long curTime = get_current_time_us();


	// function pointer
	void* (*p[8])(void* _sv);
	// point to functions
	p[0] = thread_button;
	p[1] = thread_twocolor;
	p[2] = thread_temp;
	p[3] = thread_track;
	p[4] = thread_touch;
	p[5] = thread_rgbcolor;
	p[6] = thread_aled;
	p[7] = thread_aled;


   set_by_min_freq();   // set min freq to get the wcet for low freq
	int i;
	for(i = 0; i < 8; i++) {
		// profile worse case execution time
      int j;
		for(j = 0; j < PROFILE_AMOUNT; j++) {
			long long startTime = get_current_time_us();
			(*p[i])(&sv);
			long long endTime = get_current_time_us();

			long long execTime = endTime - startTime;
			if(sv->wcet[i] < execTime) {
				sv->wcet[i]     = execTime;
				sv->execTime[i] = execTime;
			}
		}
		sv->currDeadline[i] = workloadDeadlines[i];
		printDBG("deadline: %llu   exec time: %llu us\n", workloadDeadlines[i], sv->wcet[i]);
	}
   set_by_max_freq();   // reset freq to high
}



void debugWCET(SharedVariable* sv) {
	int ii;
	printDBG("\nWCET        : ");
	for(ii = 0; ii < 8; ii++) {
		printDBG("%8llu ", sv->wcet[ii]);
	}
	printDBG("\n");
}

void debugAliveTasks(const int* aliveTasks) {
	int ii;
	printDBG("aliveTask   : ");
	for(ii = 0; ii < 8; ii++) {
		printDBG("%8d ",aliveTasks[ii]);
	}
	printDBG("\n");
}

void debugSlack(SharedVariable* sv) {
	int ii;
	printDBG("slack       : ");
	for(ii = 0; ii < 8; ii++) {
		printDBG("%8llu ", sv->slack[ii]);
	}
	printDBG("\n");
}

void debugDeadline(SharedVariable* sv) {
   int ii;
   printDBG("deadline    : ");
   for(ii = 0; ii < 8; ii++) {
      printDBG("%8llu ", sv->currDeadline[ii]);
   }
   printDBG("\n");
}

void debugAll(SharedVariable* sv, const int* aliveTasks, long long idleTime) {
   long long curTime = get_scheduler_elapsed_time_us();
   printDBG("elapsed time: %llu\n", curTime);
   debugWCET(sv);
   debugDeadline(sv);
   debugSlack(sv);
   debugAliveTasks(aliveTasks);
   printDBG("\n");
}

void updateCurrDeadline(SharedVariable* sv, const int* aliveTasks, long long elapsedTime) {
	int i;
	for(i = 0; i < 8; i++) {
		if(aliveTasks[i] == 1) {
			sv->currDeadline[i] -= elapsedTime;
		}
		else {
         sv->currDeadline[i] = workloadDeadlines[i]; 
         sv->execTime[i]     = 0;
		}
	} // end for
}

void updatePrevExecTime(SharedVariable* sv, long long elapsedTime) {
	//sv->duration[sv->prevTask] -= elapsedTime;
   sv->execTime[sv->prevTask] += elapsedTime;
}

int selectTask(SharedVariable* sv, const int* aliveTasks) {
	int 		 currSel          = -1;
	long long earliestDeadline = 9223372036854775807; // max long long
	int i;
	for(i = 0; i < 8; i++) {
      if(aliveTasks[i] == 1 && earliestDeadline > sv->currDeadline[i]) {
			earliestDeadline = sv->currDeadline[i];
			currSel          = i;
		}
	}
	return currSel;
}

int selectFreq(SharedVariable* sv, int sel) {
   long long slack = sv->currDeadline[sel] - sv->wcet[sel] - sv->execTime[sel];
	if(slack > 0) {
      //set_by_min_freq();
		return 0;
	}
   //set_by_max_freq();
	return 1;
}

// select_task(SharedVariable* sv, const int* aliveTasks):
// This function is called while runnning the actual scheduler
// - Parameters
// sv: The variable which is shared for every function over all threads
// aliveTasks: an array where each element indicates whether the corresponed task is alive(1) or not(0).
// idleTime: a time duration in microsecond. You can know how much time was waiting without any workload
//           (i.e., it's larger than 0 only when all threads are finished and not reache the next preiod.)
// - Return value
// TaskSelection structure which indicates the scheduled task and the CPU frequency
TaskSelection select_task(SharedVariable* sv, const int* aliveTasks, long long idleTime) {
	// TODO: Fill the body
	// This function is executed inside of the scheduling simulation.
    // You need to implement an energy-efficient EDF (Earliest Deadline First) scheduler.

	// Tip 1. You may get the current time elapsed in the scheduler here like:
	// long long curTime = get_scheduler_elapsed_time_us();

	// Also, do not make any interruptable / IO tasks in this function.
	// You can use printDBG instead of printf.


	// // Sample scheduler: Round robin
	// // It selects a next thread using aliveTasks.
	// static int prev_selection = -1;

	// int i = prev_selection + 1;
	// while(1) {
	// 	if (i == NUM_TASKS)
	// 		i = 0;

	// 	if (aliveTasks[i] == 1) {
	// 		prev_selection = i;
	// 		break;
	// 	}
	// 	++i;
	// }

   // Efficient Earliest Deadline First Scheduler
	long long currTime    = get_scheduler_elapsed_time_us();
   long long elapsedTime = currTime - sv->prevTime;

	updateCurrDeadline(sv, aliveTasks, elapsedTime);
	updatePrevExecTime(sv, elapsedTime);

	int task = selectTask(sv, aliveTasks);
	int freq = selectFreq(sv, task);

	sv->prevTime = currTime;
	sv->prevTask = task;

	TaskSelection sel;
	sel.task = task; // The thread ID which will be scheduled. i.e., 0(BUTTON) ~ 7(BUZZER)
	sel.freq = freq; // Request the maximum frequency (if you want the minimum frequency, use 0 instead.)

   if(idleTime > 0) {
      printDBG("\tidleTime: %llu\n", idleTime);
   }



	//_debugSlack(sv);
	//_debugAliveTasks(aliveTasks);
	//printDBG("   task: %d   freq: %d   idle: %llu\n", sel.task, sel.freq, idleTime);

   return sel;
}
